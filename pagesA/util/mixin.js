export default {
	data() {
		return {
			TabBarcurrent: 0
		}
	},
	methods: {
		switchTab(index) {
			this.TabBarcurrent = index
			if (this.TabBarcurrent === 0) {
				uni.redirectTo({
					url: '/pagesC/wallPaper/wallPaper'
				})
			} else if (this.TabBarcurrent == 1) {
				uni.redirectTo({
					url: '/pagesC/wallPaper/toplist/index'
				})
			} else if (this.TabBarcurrent == 2) {
				uni.redirectTo({
					url: '/pagesC/wallPaper/user/index'
				})
			}
		}
	}
}
