'use strict';
exports.main = async (event, context) => {
    const db = uniCloud.database();
    const res = await db.collection('fe-mql').where({
		"index":event.index
	}).get()
    return res.data
};