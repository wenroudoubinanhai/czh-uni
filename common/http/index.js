

import HaloConfig from '@/config/halo.config.js'
import {
	setInterceptors
} from "./interceptors.js";
import Request from "@/js_sdk/luch-request/luch-request";

const http = new Request()
/* 设置全局配置 */
http.setConfig((config) => {

	// 如果是在外部浏览器调试或者编译为h5，请注释该行代码
	config.baseURL = HaloConfig.apiUrl;

	config.header = {
		...config.header,
		'api-authorization': HaloConfig.apiAuthorization,
		ContentType: 'application/json',
		dataType: 'json'
	}
	return config
})
setInterceptors(http)

export {
	http
}
