
export default {
	namespaced: true,
	state: {
		journalInfo: null,
	},
	getters: {
		getJournalInfo(state) {
			return state.journalInfo
		},
	},
	mutations: {
		setJournalInfo(state, data) {
			state.journalInfo = data;
		},
	},
};
