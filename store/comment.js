
 
export default {
	namespaced: true,
	state: {
		commentInfo: {},
	},
	getters: {
		getCommentInfo(state) {
			return state.commentInfo
		},
	},
	mutations: {
		setCommentInfo(state, data) {
			state.commentInfo = data;
		},
	},
};
